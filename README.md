# IssueTrackingSystem
 
 An Issue Tracking System is a responsive website implemented with JavaEE for backend side and (Html, Css , Js, Angular, Bootstrap, AmCharts ,.. ) for frontend.
 
 What This site do?
  *  All Users can create a ticket and register it.
  *  All Users can login and register to site.
  *  All Users can see statuses of their ticket and rate their closed tickets.
  *  All Users can edit their password and profile info.
  *  There are four permission access in this system(admin , normal user, officer, professor)
  *  Admin can see various data visualization and reports.
  *  Admin can remove, add , activate unregistered users and also deactivate them.

<p style= "text-align:center; font-size:40px"> <b>Landing page </b></p>
![Screenshot__138_](/uploads/1369b8eed2c6d88fba5ce303010cdd14/Screenshot__138_.png)
![Screenshot__139_](/uploads/3c44d75b1f02c42e7cb09acfa72845fd/Screenshot__139_.png)

<p style= "text-align:center; font-size:40px"> <b>Register page </b></p>

![register](/uploads/c6f151561a0d14a973e7ee17e499ad04/register.PNG)


<p style= "text-align:center; font-size:40px"> <b>Responsiveness</b></p>

<img src="/uploads/851d834f2c59032d9fdfe07804c125b9/dd.PNG"  width="33%"  style="float:left">
<img src="/uploads/fa56dfd4b2c0771717618b128a0149cc/rs.PNG"  width="33%"  style="float:left">
<img src="/uploads/3839c9050aa706bd04b79cdb900e0d6e/lore.PNG"  width="33%"  style="float:left">



<p style= "text-align:center; font-size:40px"> <b>User Assignees</b></p>
![Screenshot__140_](/uploads/4d9de339fd3f92be6594848ecb378bae/Screenshot__140_.png)



<p style= "text-align:center; font-size:40px"> <b>Update Profile Page</b></p>
![Screenshot__141_](/uploads/70aa710e607c2281dcbfd3239785aeab/Screenshot__141_.png)

<p style= "text-align:center; font-size:40px"> <b>Create ticket Page</b></p>
![Screenshot__143_](/uploads/fd53e065a0dd2983fdf9101b644701c7/Screenshot__143_.png)


<p style= "text-align:center; font-size:40px"> <b>User Management Page</b></p>
![dds](/uploads/350fe7cbfa0b9ea09d98b5d57c1d385a/dds.PNG)


<p style= "text-align:center; font-size:40px"> <b>Report Page</b></p>
![Screenshot__151_](/uploads/b6f06342c1381ec8846f2acd26ae3c1f/Screenshot__151_.png)
![Screenshot__152_](/uploads/0caaaa3e2e3871de5c899d1d77ade0d7/Screenshot__152_.png)
![Screenshot__147_](/uploads/981e5907bc3cb7b5c73c9984e9800a25/Screenshot__147_.png)
![Screenshot__150_](/uploads/38f57bd7b95b792a342ce3564e60bbc2/Screenshot__150_.png)
![Screenshot__148_](/uploads/3472dbf9d851dcef90058d9b21b7b0be/Screenshot__148_.png)
